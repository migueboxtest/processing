int XSIZE = 300, YSIZE = 300;	//constants

void settings(){
	size(XSIZE, YSIZE);
}

void setup(){
	background(255);
}
int xpos = width/2, ypos = height/2;
float r = 0, g = 0, b = 0;	//RGB color values
int xmov = 2, ymov = 1, radius = 7;	//constants
void draw(){
	background(255);
	
	stroke(r, g, b);
	ellipse(xpos, ypos, 2*radius, 2*radius);
	fill(r ,g ,b);
	xpos += xmov;
	ypos += ymov;

	if (xpos >= width || xpos <= 0) {
		xmov = -xmov;
		r = random(255);
		g = random(255);
		b = random(255);
	}
	if (ypos >= height || ypos <= 0) {
		ymov  = -ymov;
		r = random(255);
		g = random(255);
		b = random(255);
	}
}
