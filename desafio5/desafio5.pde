int row = 3, column = 5, squares = 4;
void setup(){
	size(600,600);
	rectMode(CENTER);
	noFill();
	frameRate(24);
}

void draw(){
	background(255);
	
	int i, j, h;
	for(i = 1; i <= row; i++)
		for(j = 1; j <= column; j++)
			for(h = squares; h >= 1; h--){
				rect((j*width)/column - width/(2*column), (i*height)/row - height/(2*row), (h*width)/(column*squares) - 9, (h*height)/(row*squares) - 9);
				fill(random(255), random(255), random(255));
			}
}
