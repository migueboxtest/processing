//draws a bee, x and y are the coordinates of the center of a square, with radius being its radius; f is the current frame of a frame counter and frame is the interval in which those wings reset
void bee(float x, float y, float radius, int f, int frame) {
	ellipseMode(CENTER);
	//wings
	fill(150);
	stroke(0);
	bezier(x, y + radius / 7.0, x + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))));
	bezier(x, y + radius / 7.0, x + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x - radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))));
	//body
	stroke(255, 191, 0);
	fill(255, 191, 0);
	ellipse(x, y + radius/2.0, radius*2, (radius*2)/2.0);
	//eye
	stroke(2, 2, 0);
	fill(2, 2, 0);
	ellipse(x + (3.0 * radius) / 4.0, y + radius/2.7, radius/5.0, radius/3.0);
}

float range(float start, float arc, int f, int frame) {
return start + arc * sin(PI * (float(f) / float(frame)));
}
