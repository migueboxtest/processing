// [0] = x; [1] = y
void wasd_mov(float position[], float mov[], int time[], float radius) {
	if (keyPressed && time[0] >= 0) {
		switch(key) {
			case 'w':
			position[1] -= mov[1];
			if(position[1] + 4 * radius < 0)
				position[1] = height + 2 * radius;
			break;
			case 's':
			position[1] += mov[1];
			if(position[1] - 2 * radius > height)
				position[1] = (-2) * radius;
			break;
			case 'a':
			position[0] -= mov[0];
			if(position[0] + 2 * radius < 0)
				position[0] = width + 2 * radius;
			break;
			case 'd':
			position[0] += mov[0];
			if(position[0] - 2 * radius > width)
				position[0] = (-2) * radius;
			break;
		}
	}
}
