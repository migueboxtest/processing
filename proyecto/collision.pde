void collision(){
	int i;
	for(i = num_animals - 1; i >= 0; i--){
		if(touching(position[0], position[1], radius, combinations[1], animals[i].getX(), animals[i].getY(), animals[i].getRadius(), animals[i].getType())) {
			num_animals = 0;
			combinations[1] = int(random(1,3.99));
			break;
		}
	}
}

boolean touching(float x1, float y1, float radius1, int type1,float x2, float y2, float radius2, int type2){
	float radiusy1, radiusy2;
	switch(type1){
	case 1:
		radiusy1 = radius1;
		break;
	case 2:
		radiusy1 = radius1;
		break;
	case 3:
		radiusy1 = radius1 * 1.5;
		radius1 = (radius1 * 1.5)/1.86;
		x1 += radius1;
		y1 += radiusy1;
		break;
	default:
		return false;
	}
	switch(type2){
	case 1:
		radiusy2 = radius2;
		break;
	case 2:
		radiusy2 = radius2;
		break;
	case 3:
		radiusy2 = radius2 * 1.5;
		radius2 = (radius2 * 1.5)/1.86;
		x2 += radius2;
		y2 += radiusy2;
		break;
	default:
		return false;
	}
	int i;
	for(i = -1; i <= 1; i+= 2) {
		if(x1 + (i*radius1) <= x2 + radius2 && x1 + (i*radius1) >= x2 - radius2)
			if(y1 + (i*radiusy1) <= y2 + radiusy2 && y1 + (i*radiusy1) >= y2 - radiusy2)
				return true;
		if(x2 + (i*radius2) <= x1 + radius1 && x2 + (i*radius2) >= x1 - radius1)
			if(y2 + (i*radiusy2) <= y1 + radiusy1 && y2 + (i*radiusy2) >= y1 - radiusy1)
				return true;
	}
	return false;
}
