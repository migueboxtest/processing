int XSIZE = 300, YSIZE = 300, f = 0, frame = 60;
float x = XSIZE/2, y = YSIZE/2, radius = YSIZE/2;
void settings(){
	size(XSIZE, YSIZE);
}
void setup(){
	background(255);
	frameRate(frame);
}
void draw(){
	background(255);
	ellipseMode(CENTER);
	//wings
	fill(150);
	stroke(0);
	bezier(x, y + radius / 7.0, x + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))));
	bezier(x, y + radius / 7.0, x + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x + radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) + radius * 3.0/5.0 * cos(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))) - radius * 3.0/5.0 * sin(QUARTER_PI * 7 + HALF_PI * sin(PI * (float(f) / float(frame)))), x - radius * cos(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))), y + radius / 7.0 - radius * sin(QUARTER_PI + HALF_PI * sin(PI * (float(f) / float (frame)))));
	//body
	stroke(255, 191, 0);
	fill(255, 191, 0);
	ellipse(x, y + radius/2.0, radius*2, (radius*2)/2.0);
	//noFill();
	f++;
	if (f == 60)
		f = 0;
}
