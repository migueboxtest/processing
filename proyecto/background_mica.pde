int  background_mica(int m, float circles[], int width, int height){
	int i;
	ellipseMode (CENTER);
	noStroke();
	background (12);
	fill (56);
	rectMode(CORNER);
	rect (0, (3.0/4.0)*float(height), width, float(height)/4.0);
	
	circles[5*m] = random(width);
	circles[5*m + 1] = random(height - 200);
	circles[5*m + 2] = random(255);
	circles[5*m + 3] = random(255);
	circles[5*m + 4] = random(255);
	
	for (i = 0; i < m ; i += 1){
		fill (circles[5*i + 2], circles[5*i + 3], circles[5*i + 4]);
		ellipse (circles[5*i], circles[5*i + 1], 10, 10);
	}
	if (m == num - 1)
		mica_growth = false;
	if (m == 0)
		mica_growth = true;
	m += (mica_growth) ? 1 : -1;
	return m;
}
