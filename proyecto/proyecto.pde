//frame and time related variables
int f = 0, frame = 20, framestop = 3 * frame;
int[] time = new int[3];
float counter = 0;
//background position and variables
float x, y, area = 2;
int color_background_isa = 0;
int num = 100, m = 0;
float[] circles = new float[5 * num];
boolean mica_growth = true;
//animal movement and variables
int color_isa_animal = 0;
float[] position = new float[2], mov = {20, 20};
float radius;
//other animals
int max_animals = 6, num_animals = 0;
Animal[] animals = new Animal[6];
//combination array
int[] combinations = {int(random(1,3.99)), int(random(1,3.99))};	//{background, animal}
//images
PImage portada, warning;
boolean present = true, one_time_warning = true;

void setup() {
	background(255);
	frameRate(frame);
	size(750, 600);
	portada = loadImage("portada.png");
	warning = loadImage("warning.png");

	x = width/2;
	y = height/2;
	radius = height/7;
	position[0] = width/2.0;
	position[1] = height/2.0;
	time[2] = frame * (-1) * (10);
}

void draw(){
	if(present) {
		image(portada, 0.0, 0.0, float(width), float(height));
		if (keyPressed && key == 'y')
			present = false;
	} else {
		if(one_time_warning && time[2] == 0) {
			imageMode(CENTER);
			image(warning, float(width/2), float(height/2), float(width/2), float(height/2));
			if(mouseX <= (3*width)/4 && mouseX >= width/4 && mouseY <= (3*height)/4 && mouseY >= height/4 && mousePressed)
				one_time_warning = false;
		} else {
			//biome
			switch(combinations[0]) {
				case 1:
					background_migue(x, y, area, width, height, f, framestop);
					break;
				case 2:
					background_isa(color_background_isa);
					break;
				case 3:
					m = background_mica(m, circles, width, height);
					break;
			}
			//background animals
			int i;
			for(i = num_animals - 1; i >= 0; i--){
				animals[i].display();
				animals[i].move();
			}
			//animal
			switch(combinations[1]) {
				case 1:
					animal_migue(position[0], position[1], radius, f, framestop);
					break;
				case 2:
					animal_isa(position[0], position[1], radius, color_isa_animal);
					if(keyPressed && key == ' ') {
						color_isa_animal += 10;
						if (color_isa_animal > 255)
							color_isa_animal = 0;
					}
					break;
				case 3:
					animal_mica(position[0], position[1],  (3.0*radius)/1.86,3*radius);
					break;
			}
			collision();
			if (time[0] < 0)
				flashbang(time);
			wasd_mov(position, mov, time, radius);
			keys(combinations, time);

			if (++f == framestop)
				f = 0;
			counter(time);
		}
	}
}
