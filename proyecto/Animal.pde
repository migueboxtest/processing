class Animal{
	float x, y, radius, xmov, ymov;
	int type, color_isa_animal;
	Animal(float tmpx, float tmpy, float tmpradius, int tmpcolor_isa_animal, int tmptype){
		x = tmpx;
		y = tmpy;
		radius = tmpradius;
		color_isa_animal = tmpcolor_isa_animal;
		type = tmptype;
		xmov = random(1, 10);
		ymov = random(1, 10);
	}
	void display(){
		switch(type) {
			case 1:
				animal_migue(x, y, radius, f, framestop);
				break;
			case 2:
				animal_isa(x, y, radius, color_isa_animal);
				if (keyPressed && color_isa_animal > 255)
					color_isa_animal = 0;
				break;
			case 3:
				animal_mica(x, y,  (3.0*radius)/1.86,3*radius);
				break;
		}
	}
	void move(){
		xmov = (x > width || x < 0) ? (-1)*xmov : xmov;
		ymov = (y > height || y < 0) ? (-1)*ymov : ymov;
		x += xmov;
		y += ymov;
	}
	float getX() {
		return x;
	}
	float getY() {
		return y;
	}
	float getRadius() {
		return radius;
	}
	int getType() {
		return type;
	}
}
