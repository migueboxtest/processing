void animal_mica(float x, float y, float rx, float ry) {
  noStroke();
  rectMode(CORNER);
    //pelo
  fill(#89B212);
    rect(x + rx * 0.083, y, rx * 0.083, ry * 0.18);
    rect(x + rx * 0.83, y, rx * 0.083, ry * 0.18);
    rect(x + rx * 0.26, y + ry * 0.041, rx * 0.083, ry * 0.13);
    rect(x + rx * 0.46, y, rx * 0.083, ry * 0.13);
    rect(x + rx * 0.64, y + ry * 0.041, rx * 0.083, ry * 0.13);
    //cuerpo
  fill (#54A057);
    rect(x + rx * 0.33, y + ry * 0.49, rx * 0.375, ry * 0.36);
    //cabeza
 fill(#B6ED59);
    ellipse(x + rx * 0.5, y + ry * 0.34, rx * 1, ry * 0.54);
    //ojos
  fill(255);
    ellipse(x + rx * 0.33, y + ry * 0.28, rx * 0.167, ry * 0.09);
    ellipse(x + rx * 0.67, y + ry * 0.28, rx * 0.167, ry * 0.09);
    //nariz
    ellipse(x + rx * 0.5, y + ry * 0.43, rx * 0.083, ry * 0.04);
    //pupilas
  fill(0);
    ellipse(x + rx * 0.32, y + ry * 0.289, rx * 0.083, ry * 0.04);
    ellipse(x + rx * 0.69, y + ry * 0.289, rx * 0.083, ry * 0.04);
    //cuerpo abajo
  fill(#AAF2A4);
    rect(x + rx * 0.29, y + ry * 0.82, rx * 0.46, ry * 0.18);  
}
