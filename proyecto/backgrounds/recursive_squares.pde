// initializer, and relates the background to the frames
void background_migue(float x, float y, float area_ratio, int width, int height, int f, int frame) {
	float side = (width > height) ? float(width) : float(height);
	side *= exp(log(area_ratio) * (2 * float (f) / float (frame)));
	int c = 0;
	recursive_squares(x, y, area_ratio, side, c);
}
// creates squares with a given area ratio, with "area_ratio" being its inverse, therefore, values are greater than 1
void recursive_squares(float x, float y, float area_ratio, float l, int c) {
	rectMode(CENTER);
	if (c++ % 2 == 0) {
		stroke(50);
		fill(50);
	}
	else {
		stroke(150);
		fill(150);
	}
	rect(x, y, l, l);
	l = l / sqrt(area_ratio);
	if (int(l) > 1) {
		recursive_squares(x, y, area_ratio, l, c);
	}
}
