int x, p;
void setup()
{
  int i;
  size(400,400);  
  background(255,255,255);
  strokeWeight(56);
  stroke(255,0,0);
  line(0,190,400,320);
  /*
  for (i = 0; i < 100; i++)
  {
    line(0,100+i,400,150+i);
  }
*/
  frameRate(400);
}

void draw()
{
  int i;
  strokeWeight(1);
  for (i = 0; i < 256; i++)
  {
    stroke(i,x,p);
    line(0,130+x,400,320+p);
    x++;
    if (x == 270)
    {
      x = -130;
    }
    p++;
    if (p == 270)
    {
      p = -320;
    }
  }
  circle(200,200,50);
}
