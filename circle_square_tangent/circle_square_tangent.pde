int XSIZE = 400, YSIZE = 400, rightmove = 1, leftmove = 1;	// constants

int xposition, yposition, radius;

void settings(){
	size(XSIZE, YSIZE);
}

void setup() {
	background(255);
	
	xposition = XSIZE/2;
	yposition = YSIZE/2;
	if (xposition < yposition)
		radius = (3*xposition)/4;
	else
		radius = (3*yposition)/4;

	rectMode(CENTER);
}

int i = 0;
boolean right = true;
void draw() {
	background(255);
	if (i > xposition - radius && i < xposition + radius)
		rect(xposition, yposition, 2*radius, 2*radius);
	else
		ellipse(xposition, yposition, 2*radius, 2*radius);

	line(i, 0, i, YSIZE);
	i += (right) ? rightmove : -leftmove; 
	
	if (i <= 0)
		right = true;
	if (i >= XSIZE)
		right = false;
}
