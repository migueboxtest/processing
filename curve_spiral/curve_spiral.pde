float x1, centerx, centery, y;
void setup() {
	size(600,600);
	centerx = width/2;
	centery = height - height/20;
	x1 = centerx;
	y = height/20;
	noFill();
	strokeWeight(3);
}
void draw(){
	int i;
	background(0);
	stroke(random(255), random(255), random(255));
	for (i = 0; i <= 5; i++)
		bezier(centerx, centery, centerx + i*(mouseX - x1)/5, centery, x1 + i*(mouseX - x1)/5 + i*(mouseX - x1)/5, y, x1 + i*(mouseX - x1)/5, y);
}
