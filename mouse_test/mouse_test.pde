int XSIZE = 300, YSIZE = 300;
int x;

void settings() {
	size(XSIZE, YSIZE);
}

void setup() {
	x = int(random(width));
	background(255);
}

void draw() {
	int r = int(random(255));
	int g = int(random(255));
	int b = int(random(255));
	noStroke();
	ellipse(mouseX, mouseY, x/5, x/5);
	fill (r, g, b);
}
